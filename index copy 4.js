const rpio = require('rpio');

// Configura los pines GPIO
const CLOCK_PIN = 38; // El pin GPIO al que está conectado el reloj
const DATA_PIN = 40; // El pin GPIO al que está conectado el dato

// Configura los pines GPIO como entrada
rpio.open(CLOCK_PIN, rpio.INPUT);
rpio.open(DATA_PIN, rpio.INPUT);

let dataValue = 0; // Almacena el dato completo
let bitsRead = 0; // Contador de bits leídos
let lastPulseTime = Date.now(); // Tiempo del último pulso

// Función para manejar cada pulso del reloj
function handleClockPulse() {
    const dataBit = rpio.read(DATA_PIN);

    // Hace espacio para el nuevo bit y lo agrega al dato
    dataValue = (dataValue << 1) | dataBit;

    bitsRead++;

    // Si hemos leído 24 bits, mostramos el dato completo
    if (bitsRead === 24) {
        console.log('Dato recibido:', dataValue);
        console.log(!dataBit);

        // Reinicia los contadores
        dataValue = 0;
        bitsRead = 0;
    }

    // Calcula el tiempo transcurrido desde el último pulso
    const currentTime = Date.now();
    const pulseInterval = currentTime - lastPulseTime;

    // Actualiza el tiempo del último pulso
    lastPulseTime = currentTime;

    // Si el tiempo entre pulsos es mayor que 200ms, reinicia dataValue a 0
    if (pulseInterval > 200) {
        dataValue = 0;
        console.log('No se detectaron pulsos, dataValue reiniciado a 0');
    }
}

// Configura una interrupción para el pin del reloj
rpio.poll(CLOCK_PIN, handleClockPulse);
