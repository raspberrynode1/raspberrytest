// import { Gpio  } from 'pigpio';
// const Gpio = _Gpio;
// import { createRequire } from 'module';
// const require = createRequire(import.meta.url)

import { Gpio } from 'pigpio';
// const Gpio = pigpio.Gpio;


const pin = 12; // El número del pin GPIO que estás utilizando
const frequencyRange = { min: 1, max: 100 }; // Rango de frecuencia deseado (en Hz)
const dutyCycleRange = { min: 0, max: 255 }; // Rango de ciclo de trabajo deseado (0-255)

const pwm = new Gpio(pin, { mode: Gpio.OUTPUT });

// Función para configurar la frecuencia y el ciclo de trabajo
function setPWM(frequency, dutyCycle) {
    pwm.hardwarePwmWrite(frequency, dutyCycle);
}

// Ejemplo de cambio gradual de frecuencia
function sweepFrequency() {
    let frequency = frequencyRange.min;
    const step = 1; // Paso de frecuencia

    const interval = setInterval(() => {
        setPWM(frequency, 500000); // Fija el ciclo de trabajo a la mitad (128)
        console.log(`Frecuencia: ${frequency} Hz`);
        frequency += step;

        if (frequency > frequencyRange.max) {
            clearInterval(interval);
        }
    }, 500); // Intervalo de cambio de frecuencia en milisegundos
}

// Inicia la función de cambio gradual de frecuencia
sweepFrequency();


process.on('SIGINT', () => {
    console.log("SIGINT");
    process.exit();
});

process.on('exit', (code) => {
    console.log('Closing Raspberry PI');
    pwm.hardwarePwmWrite(0, 0)
    // process.exit(0);
});




