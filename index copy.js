const Gpio = require('onoff').Gpio;
const led = new Gpio(4, 'out');
const blink = setInterval(() => {
    led.writeSync(led.readSync() ^ 1);
}, 250);


// setTimeout(() => {

// }, 5000);

process.on('SIGINT', _ => {
    console.log('voy pa juera');
    clearInterval(blink);
    led.writeSync(0);
    led.unexport();
})


