const Gpio = require('pigpio').Gpio;

const clk = new Gpio(26, {
    mode: Gpio.INPUT,
    pullUpDown: Gpio.PUD_UP,
    edge: Gpio.RISING_EDGE,
    timeout: 200
});
const data = new Gpio(19, {
    mode: Gpio.INPUT,
    pullUpDown: Gpio.PUD_UP,
});

let lastPulseTime = Date.now();

// Level must be stable for 10 ms before an alert event is emitted.
// clk.glitchFilter(20000);

clk.on('interrupt', (level, tick) => {
    // console.log('level: ', level);
    if (level === 1) {
        clk.disableInterrupt();
        const pulseInterval = tick - lastPulseTime;
        let dataRead = 0;
        for (let i = 0; i < 24; i++) {
            while (clk.digitalRead()) {
                // if (Date.now() - tick > 650) {
                //     this.error++;
                //     return;
                // }

            }
            dataRead >>= 1;
            dataRead += data.digitalRead() ? 0 : 0x400000;
            while (!clk.digitalRead()) {
                // if (Date.now() - tick > 650) {
                //     this.error++;
                //     return;
                // }

            }
        }

        // console.log('tick_diff: ', pulseInterval);
        console.log('dataRead: ', dataRead);
        lastPulseTime = tick;
        clk.enableInterrupt(Gpio.RISING_EDGE, 200);
        // console.log(++count);
    } else if (level === 2) {
        console.log('No hay indicador');
    }
});


// const Gpio = require('pigpio').Gpio;

// const led = new Gpio(17, { mode: Gpio.OUTPUT });
// const button = new Gpio(26, {
//     mode: Gpio.INPUT,
//     pullUpDown: Gpio.PUD_UP,
//     edge: Gpio.EITHER_EDGE
// });

// for (let i = 0; i < 500000; i++) {
//     console.log("a" + i);
// }

// button.on('interrupt', (level) => {
//     led.digitalWrite(level);
//     console.log("boooo");

// });