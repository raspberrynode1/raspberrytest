// const { Gpio } = require('pigpio');

const Gpio = require('pigpio').Gpio;

class Indicador {
    static dataValue = 0;
    static error = 11;

    constructor(clkPin = 19, dataPin = 13, resolution = 1000, toFixed = 3) {
        this.clkPin = clkPin;
        this.dataPin = dataPin;
        this.lastPulseTime = Date.now();
        this.resolution = resolution;
        this.fixed = toFixed;

        this._clk = new Gpio(clkPin, {
            mode: Gpio.INPUT,
            pullUpDown: Gpio.PUD_UP,
            edge: Gpio.RISING_EDGE,
            timeout: 200,
            alert: true
        });
        this._data = new Gpio(dataPin, {
            mode: Gpio.INPUT,
            pullUpDown: Gpio.PUD_UP,
            alert: false
        });
    }

    get indicador() {
        return this.dataValue
    }

    sleepES5(ms = 10) {
        const esperarHasta = Date.now() + ms;
        while (Date.now() < esperarHasta) continue;
    }

    checkClkTimeout() {
        const currentTime = Date.now();
        const pulseInterval = currentTime - this.lastPulseTime;
        if (pulseInterval > 200) { // Cambia el tiempo de espera según tus necesidades
            this.error = 11;
        }
    }

    poll() {
        pigpio.waitForEdge(this.clkPin, pigpio.RISING_EDGE, (level, tick) => {
            const startTime = Date.now(); // Tiempo de inicio del proceso de lectura
            const pulseInterval = startTime - this.lastPulseTime;
            let dataRead = 0;
            for (let i = 0; i < 24; i++) {
                while (pigpio.read(this.clkPin)) {
                    if (Date.now() - startTime > 130) {
                        this.error++;
                        return;
                    }

                }
                dataRead >>= 1;
                dataRead += pigpio.read(this.dataPin) ? 0 : 0x400000;
                while (!pigpio.read(this.clkPin)) {
                    if (Date.now() - startTime > 130) {
                        this.error++;
                        return;
                    }

                }
            }
            this.lastPulseTime = startTime;
            if (pulseInterval > 130) {
                this.error++;
                return;
            }

            if (dataRead >= 0x100000) {
                dataRead ^= 0x100000;
                dataRead *= -1;
            }
            this.dataValue = (dataRead / this.resolution).toFixed(this.fixed);
            this.error = 0;
        });

        setInterval(() => {
            this.checkClkTimeout();
        }, 200);
    }
}

module.exports = Indicador;
