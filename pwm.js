const rpio = require('rpio');

const pin = 12; // El número del pin GPIO que estás utilizando
const range = 5000; // Frecuencia máxima en Hz
const step = 100; // Paso de frecuencia en Hz

// Inicializa el pin GPIO para la salida PWM
rpio.init({ gpiomem: false, close_on_exit: false })
rpio.open(pin, rpio.PWM);
// rpio.close(pin);
// Función para establecer la frecuencia PWM
function setPWMFrequency(frequency) {
    rpio.close(pin);
    rpio.open(pin, rpio.PWM);

    rpio.pwmSetClockDivider(8); // Establece el divisor del reloj PWM
    rpio.pwmSetRange(pin, frequency);
    rpio.pwmSetData(pin, Math.floor(frequency / 2)); // Ciclo de trabajo al 50% inicialmente
}

// Ejemplo de cambio gradual de frecuencia
function sweepFrequency() {
    let frequency = 100;

    const interval = setInterval(() => {
        setPWMFrequency(frequency);
        console.log(`Frecuencia: ${frequency} Hz`);
        frequency += step;

        if (frequency > range) {
            clearInterval(interval);
        }
    }, 2000); // Intervalo de cambio de frecuencia en milisegundos
}

// Inicia el cambio gradual de frecuencia
sweepFrequency();


process.on('SIGINT', () => {
    console.log("SIGINT");
    process.exit();
});

process.on('exit', (code) => {
    console.log('Closing Raspberry PI');
    rpio.close(pin, rpio.PIN_RESET);
    rpio.exit();
    // process.exit(0);
});

