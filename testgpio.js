const pigpio = require('pigpio');
const Gpio = pigpio.Gpio;

pigpio.configureClock(5, pigpio.CLOCK_PCM);


class ClockDataReader {
    constructor(clkPin, dataPin, callback) {
        this.clkPin = new Gpio(clkPin, {
            mode: Gpio.INPUT,
            edge: Gpio.FALLING_EDGE,
            timeout: 1000,
            pullUpDown: Gpio.PULL_UP
        });
        this.dataPin = new Gpio(dataPin, {
            pullUpDown: Gpio.PULL_UP,
            mode: Gpio.INPUT
        });
        this.callback = callback;
        this.bitsReceived = 0;
        this.data = 0;
        this.initialized = false;
        this.clkPin.on('interrupt', this._clkCallback);
        this.tickPast = 0;
        this.count = 0;
    }

    _firstClkCallback(level) {
        if (!this.initialized) {
            this.initialized = true;
            console.log('First rising edge detected, starting data collection...');
            this.clkPin.off('interrupt', this._firstClkCallback);
            this.clkPin.on('interrupt', this._clkCallback.bind(this));
        }
    }

    _clkCallback(level, tick) {

        if (level === 0) {  // Ensure we're on a rising edge
            const tick_diff = tick - this.tickPast;
            this.tickPast = tick;
            if (tick_diff < 1000) {
                this.count++;
            } else {
                console.log(tick_diff, this.count);
                this.count = 0;

            }

        } else if (level === 2) {
            console.log("no hay indicador");
        }
    }
}

function myCallback(data) {
    console.log(`Data received: ${data}`);
}

const clkPin = 19;  // Pin GPIO para el reloj
const dataPin = 13;  // Pin GPIO para los datos

const reader = new ClockDataReader(clkPin, dataPin, myCallback);

process.on('SIGINT', () => {
    reader.clkPin.digitalWrite(0); // Optional: Reset pin state
    reader.dataPin.digitalWrite(0); // Optional: Reset pin state
    pigpio.terminate();
    process.exit();
});
