const rpio = require('rpio');

class Indicador {
    static dataValue = 0;
    static error = 11;

    constructor(clkPin = 38, dataPin = 40, resolution = 1000, toFixed = 3) {
        this.clkPin = clkPin;
        this.dataPin = dataPin;
        this.lastPulseTime = Date.now();
        this.resolution = resolution;
        this.fixed = toFixed;
        rpio.open(this.clkPin, rpio.INPUT);
        rpio.open(this.dataPin, rpio.INPUT);
    }

    get indicador() {
        if (this.error > 10) {
            return "No hay lectura del indicador";
        }
        return this.dataValue
    }

    checkClkTimeout() {
        const currentTime = Date.now();
        const pulseInterval = currentTime - this.lastPulseTime;
        if (pulseInterval > 200) { // Cambia el tiempo de espera según tus necesidades
            this.error = 11;
        }
    }

    poll() {
        rpio.poll(this.clkPin, _ => {
            const startTime = Date.now(); // Tiempo de inicio del proceso de lectura
            const pulseInterval = startTime - this.lastPulseTime;
            let dataRead = 0;
            for (let i = 0; i < 24; i++) {
                while (rpio.read(this.clkPin)) {
                    if (Date.now() - startTime > 130) {
                        this.error++;
                        return;
                    }

                }
                dataRead >>= 1;
                dataRead += rpio.read(this.dataPin) ? 0 : 0x400000;
                while (!rpio.read(this.clkPin)) {
                    if (Date.now() - startTime > 130) {
                        this.error++;
                        return;
                    }

                }
            }
            this.lastPulseTime = startTime;
            if (pulseInterval > 130) {
                this.error++;
                return;
            }

            if (dataRead >= 0x100000) {
                dataRead ^= 0x100000;
                dataRead *= -1;
            }
            this.dataValue = (dataRead / this.resolution).toFixed(this.fixed);
            this.error = 0;
        }, rpio.POLL_HIGH);

        setInterval(() => {
            this.checkClkTimeout();
        }, 200);
    }



}

module.exports = Indicador;