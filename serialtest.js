const SerialPort = require('serialport');

// Reemplaza '/dev/ttyS0' con el puerto serial de tu Raspberry Pi
const port = new SerialPort('/dev/ttyS0', {
  baudRate: 9600
});

// Crear un buffer para almacenar los datos entrantes
let buffer = Buffer.alloc(0);

// Evento para cuando se abre el puerto
port.on('open', () => {
  console.log('Puerto serial abierto');

});
// Enviar el comando 0xA1 cada segundo
setInterval(() => {
  const command = Buffer.from([0x52]);
  port.write(command, (err) => {
    if (err) {
      return console.error('Error al enviar el comando:', err);
    }
    console.log('Comando 0xA1 enviado');
  });
}, 1000);

// Evento para cuando se reciben datos
port.on('data', (data) => {
  // console.log(data);
  buffer = Buffer.concat([buffer, data]);
  while (buffer.length >= 13) {
    processReceivedData(buffer.subarray(1, 9));
    buffer = buffer.subarray(13);
  }
});

// Función para procesar los datos recibidos
function processReceivedData(data) {
  // Asegúrate de que los datos sean un buffer de 13 bytes
  // Convertir cada parte de los datos del buffer a valores enteros
  console.log(data);
  console.log(data.readUInt8(1));

  // Imprimir los valores
  console.log('Value 1:', convData(data.readIntBE(0,3)));
  console.log('Value 2:', convData(data.readIntBE(4,3)));
  // console.log('Value 2:', value2);
}

const convData = (dataRead) => {
  if (dataRead >= 0x100000) {
    dataRead ^= 0x100000;
    dataRead *= -1;
  }
  return dataRead;
}

// Manejo de errores
port.on('error', (err) => {
  console.error('Error de puerto:', err);
});
