const rpio = require('rpio');
const Indicador = require('./helpers/indicador');

// Configura los pines GPIO
const CLOCK_PIN = 35; // El pin GPIO al que está conectado el reloj
const DATA_PIN = 33; // El pin GPIO al que está conectado el dato

// Configura los pines GPIO como entrada
rpio.open(CLOCK_PIN, rpio.INPUT);
rpio.open(DATA_PIN, rpio.INPUT);

let dataValue = 0; // Almacena el dato completo
let lastPulseTime = Date.now(); // Tiempo del último pulso

// Función para manejar cada pulso del reloj

// const indicador = new Indicador(38, 40);
// indicador.poll();

// // console.log(indicador);
// while (true) {

// }

const handleClockPulse = _ => {
    const startTime = Date.now(); // Tiempo de inicio del proceso de lectura
    const pulseInterval = startTime - lastPulseTime;
    for (let i = 0; i < 24; i++) {
        while (rpio.read(CLOCK_PIN)) {
            if (Date.now() - startTime > 130) {
                dataValue = 0;
                return;
            }
        }
        dataValue >>= 1;
        dataValue += rpio.read(DATA_PIN) ? 0 : 0x400000;
        while (!rpio.read(CLOCK_PIN)) {
            if (Date.now() - startTime > 130) {
                dataValue = 0;
                return;
            }
        }
    }
    lastPulseTime = startTime;
    if (pulseInterval > 130) {
        dataValue = 0;
        return;
    }
    if (dataValue >= 0x100000) {
        dataValue ^= 0x100000;
        dataValue *= -1;
    }
    console.log('Dato recibido: ', dataValue);
    console.log('Tiempo transcurrido: ', pulseInterval, 'ms');
}

rpio.poll(CLOCK_PIN, handleClockPulse, rpio.POLL_HIGH);
