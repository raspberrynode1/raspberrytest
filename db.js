import { JSONPreset, JSONSyncPreset } from 'lowdb/node'
const defaultData = { posts: [] }
const db =  JSONSyncPreset('db.json', defaultData)

// Create and query items using plain JavaScript
db.data.posts.push('hello world1')
const firstPost = db.data.posts[0]

// If you don't want to type db.data everytime, you can use destructuring assignment
const { posts } = db.data
posts.push('hello world2')

// Finally write db.data content to file
db.write()